package schoolprint.PrintJobs;

import schoolprint.Utilities;
import schoolprint.interfaces.IPrintJob;

/**
 * @author MatthewBaldwin
 * 
 * A PrintJob object holds data about the system's print jobs
 *
 */
public abstract class PrintJobBase implements IPrintJob {
    
    private int BlackWhitePages;
    private int ColorPages;
    private PaperSize PaperSize;
    protected boolean DoubleSided;
    
    /**
     * Constructor for the PrintJob object
     * 
     * @param blackWhitePages The number of pages in the job which are to printed in B&W
     * @param colorPages The number of pages in the job which are to printed in colour
     * @param doubleSided A flag indicating whether to print double sided or not
     */
    public PrintJobBase(int blackWhitePages, int colorPages, PaperSize paperSize, boolean doubleSided) {
        BlackWhitePages = blackWhitePages;
        ColorPages = colorPages;
        PaperSize = paperSize;
        DoubleSided = doubleSided;
    }
    
    @Override
    public int totalPages() {
        return BlackWhitePages + ColorPages;
    }
    
    /**
     * Calculates the cost of printing a black & white page for the job.
     * 
     * @return the cost of printing ONE black & white page.
     */
    protected abstract double blackWhitePageCost();
    
    /**
     * Calculates the cost of printing a colour page for the job.
     * 
     * @return the cost of printing ONE colour page.
     */
    protected abstract double colorPageCost();

    @Override
    public double totalCost() {
        return Utilities.round(
                BlackWhitePages * blackWhitePageCost() + ColorPages * colorPageCost(), 2);
    }

    @Override
    public String toString() {
        return "Print Job Details:\n" +
                "    Page Size:  " + PaperSize + "\n" +
                "    Total Pages:  " + totalPages() + "\n" +
                "    Black & White Pages:  " + BlackWhitePages + "\n" +
                "    Colour Pages:  " + ColorPages + "\n" +
                "    Double Sided:  " + DoubleSided + "\n" +
                "    Total Cost:  $" + totalCost() + "\n";
    }

    @Override
    public int hashCode() {
        
        final int prime = 31;
        int result = 1;
        
        result = prime * result + BlackWhitePages;
        result = prime * result + ColorPages;
        result = prime * result + (DoubleSided ? 1231 : 1237);
        
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        
        PrintJobBase other = (PrintJobBase) obj;
        
        if (BlackWhitePages != other.BlackWhitePages) return false;
        if (ColorPages != other.ColorPages) return false;
        if (DoubleSided != other.DoubleSided) return false;
        
        return true;
    }
}
