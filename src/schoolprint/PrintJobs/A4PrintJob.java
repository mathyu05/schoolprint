package schoolprint.PrintJobs;

/**
 * @author MatthewBaldwin
 *
 */
public class A4PrintJob extends PrintJobBase {

    public A4PrintJob(int blackWhitePages, int colorPages, boolean doubleSided) {
        super(blackWhitePages, colorPages, PaperSize.A4, doubleSided);
    }

    @Override
    protected double blackWhitePageCost() {
        if (DoubleSided) {
            return A4BlackWhiteDoubleSidePrice;
        } else {
            return A4BlackWhiteSingleSidePrice;
        }
    }

    @Override
    protected double colorPageCost() {
        if (DoubleSided) {
            return A4ColorDoubleSidePrice;
        } else {
            return A4ColorSingleSidePrice;
        }
    }

}
