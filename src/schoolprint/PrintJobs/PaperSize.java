package schoolprint.PrintJobs;

import java.io.IOException;

/**
 * @author MatthewBaldwin
 *
 */
public enum PaperSize {
    
    // Values Start //
    
    A4; // A5, A3, etc.
    
    // Values End //
    
    /**
     * Get the PaperSize value from the given string.
     * 
     * @param paperSize The string to be converted.
     * @return The matched PaperSize value.
     * @throws IOException Thrown when the given string cannot be matched to a PaperSize value.
     */
    public static PaperSize parseString (String paperSize) throws IOException {
        if (paperSize.equals(PaperSize.A4.toString())) {
            return PaperSize.A4;
        } else {
            throw new IOException();
        }
            
    }
    
    @Override
    public String toString() {
        if (this.equals(PaperSize.A4)) {
            return "A4";
        } else {
            return "A4";
        }
    }
}
