package schoolprint;

import java.io.*;
import java.util.*;
import schoolprint.PrintJobs.PaperSize;
import schoolprint.interfaces.*;

/**
 * @author MatthewBaldwin
 *
 * The School Print object.
 * Holds data for all the print jobs currently in the system.
 */
public class SchoolPrint implements ISchoolPrint, IDataSchema {
    
    private List<IPrintJob> PrintJobs = new ArrayList<IPrintJob>();
    
    /**
     * Constuctor.
     */
    public SchoolPrint () {}
    
    @Override
    public boolean parseFile(String filename) {
        
        FileReader fileReader = null;
        BufferedReader bufferedReader = null;
        try {
            fileReader = new FileReader(filename);
            bufferedReader = new BufferedReader(fileReader);

            String line;
            boolean firstLine = true;
            while ((line = bufferedReader.readLine()) != null) {
                String[] data = Utilities.trimWhiteSpace(line.split(","));
                
                if (data.length != Schema.length) {
                    throw new IOException();
                }
                
                if (firstLine) {
                    // Verify data
                    if (!Arrays.equals(data, Schema)) {
                        throw new IOException();
                    }
                    
                    firstLine = false;
                } else {
                    // Create Print Jobs
                    int colorPages = Integer.parseInt(data[1]);
                    int blackWhitePages = Integer.parseInt(data[0]) - colorPages;
                    boolean doubleSided = Utilities.parseBool(data[2]);
                    PaperSize paperSize = PaperSize.A4; // Use "PaperSize.parseString(data[3]) when adding new page sizes ;)
                    
                    PrintJobs.add(Utilities.printJobChooser(blackWhitePages, colorPages, doubleSided, paperSize));
                }
            }
            
            if (PrintJobs.isEmpty()) {
                System.out.println("The File contains no validPrint Jobs.");
                return false;
            }
            
            return true;
            
        } catch (FileNotFoundException e) {
            // No file
            System.err.println("No file could be found at \'" + filename + 
                                "\'.  Please check your file name is valid.");
            return false;
        } catch (IOException | NumberFormatException e) {
            // Problem with the file format
            System.err.println("The file given has its data arranged in such a way that this program doesn't understand." +
                                "  Please ensure the data given is in the correct format.");
            return false;
        } finally {
            
            // Try catch ugliness :(
            
            if (bufferedReader != null){
                try { bufferedReader.close(); }
                catch (IOException e) {
                    fileReaderCloser(fileReader);
                }
            } else {
                fileReaderCloser(fileReader);
            }
            
        }
    }
    
    /**
     * Extracted from above to reduce duplication
     * @param reader a reader
     */
    private void fileReaderCloser (Reader reader) {
        if (reader != null){
            try { reader.close(); } catch (IOException e) { }
        }
    }

    /**
     * Getter for the PrintJobs list
     * @return The list of PrintJobs currently in the system.
     */
    public List<IPrintJob> getPrintJobs() {
        return PrintJobs;
    }
    
    @Override
    public double totalCost() {
        
        double totalCost = 0;
        
        for (IPrintJob printJob : PrintJobs) {
            totalCost += printJob.totalCost();
        }
        
        return Utilities.round(totalCost, 2);
    }

    @Override
    public String toString() {
        
        String output = "";
        
        for (int i = 0; i < PrintJobs.size(); ++i) {
            output += "\nPRINT JOB #" + (i + 1) + "\n    " + PrintJobs.get(i).toString();
        }

        output += "\nTOTAL COST OF ALL PRINT JOBS:  $" + totalCost() + "\n";
        
        return output;
    }

    @Override
    public int hashCode() {
        
        final int prime = 31;
        int result = 1;
        
        result = prime * result + ((PrintJobs == null) ? 0 : PrintJobs.hashCode());
        
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        
        SchoolPrint other = (SchoolPrint) obj;
        
        if (PrintJobs == null) {
            if (other.PrintJobs != null) return false;
        } else if (!PrintJobs.equals(other.PrintJobs)) {
            return false;
        }
        
        return true;
    }
}
