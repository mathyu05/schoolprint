package schoolprint.interfaces;

/**
 * @author MatthewBaldwin
 * 
 * An interface holding information for PrintJobs.
 *
 */
public interface IPrintJob {    
    
    // Print Job Prices start //
    
    double A4BlackWhiteSingleSidePrice = 0.15;
    double A4BlackWhiteDoubleSidePrice = 0.10;
    
    double A4ColorSingleSidePrice = 0.25;
    double A4ColorDoubleSidePrice = 0.20;
    
    // Print Job Prices end //
    
    /**
     * Calculates the total number of pages in the print job.
     * @return The total number of pages required for the job.
     */
    int totalPages();
    
    /**
     * Calculates the total cost of the print job.
     * @return The total cost of the job in dollars.
     */
    double totalCost();
}
