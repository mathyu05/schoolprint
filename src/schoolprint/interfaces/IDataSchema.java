package schoolprint.interfaces;

/**
 * @author MatthewBaldwin
 *
 *  An Interface defining the format of the data being passed to the program.
 */
public interface IDataSchema {

    /**
     * Schema is the expected format of the job data which is passed into the program.
     * Add more fields here when more parameters are need for complex jobs (i.e. different page sizes).
     * 
     * TODO Improve robustness of data matching with this schema.  Matching Strings isn't great.
     */
    String[] Schema = {"Total Pages", "Color Pages", "Double Sided"};
}
