package schoolprint.interfaces;

import java.io.*;

/**
 * @author MatthewBaldwin
 * 
 * An interface holding information for the School Print program.
 *
 */
public interface ISchoolPrint {
    
    /**
     * Parses the data in the file passed to the program.
     * 
     * @param filename The name of the file to be parsed.
     * @throws FileNotFoundException Thrown when filename cannot be found.
     * @throws IOException Thrown when the file given is in a bad format.
     * 
     * @return true if file parsed successfully, false otherwise.
     */
    boolean parseFile(String filename);
    
    /**
     * Calculates the total cost of all print jobs in the system.
     * @return the total cost of ALL print jobs in the system dollars.
     */
    double totalCost();
}
