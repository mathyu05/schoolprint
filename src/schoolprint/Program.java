package schoolprint;

import schoolprint.interfaces.ISchoolPrint;

/**
 * @author MatthewBaldwin
 *
 */
public class Program {

    /**
     * @param args The file which contains the print jobs to process
     * 
     * Main function to process print jobs and output job details. 
     */
    public static void main(String[] args) {
        
        if (!hasCorrectArgs(args)) {
            System.err.println("Bad arguments.");
            System.out.println("Usage: <Program name> <filename>");
            return;
        }
        
        ISchoolPrint schoolPrint = new SchoolPrint();
        boolean parsedSuccessfully = schoolPrint.parseFile(args[0]);

        if (parsedSuccessfully){
            System.out.println(schoolPrint.toString());
        }
    }

    /**
     * 
     * @param args The file which contains the print jobs to process
     *
     * @return True if arguments given are looking good, false otherwise.
     */
    private static boolean hasCorrectArgs(String[] args) {
        
        if (args.length != 1) {
            return false;
        }
        
        return true;
    }

}
