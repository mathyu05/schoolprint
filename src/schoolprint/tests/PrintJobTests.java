package schoolprint.tests;

import static org.junit.Assert.*;
import org.junit.Test;
import schoolprint.PrintJobs.A4PrintJob;
import schoolprint.PrintJobs.PrintJobBase;

/**
 * @author MatthewBaldwin
 *
 */
public class PrintJobTests {

    @Test
    public void equalsTest() {
        // Arrange
        PrintJobBase pj1 = new A4PrintJob(10, 10, true);
        PrintJobBase pj2 = new A4PrintJob(20, 10, true);
        PrintJobBase pj3 = new A4PrintJob(10, 20, true);
        PrintJobBase pj4 = new A4PrintJob(10, 10, false);
        PrintJobBase pj5 = new A4PrintJob(10, 10, true);
        
        // Act & Assert
        assertTrue(pj1.equals(pj1));
        assertFalse(pj1.equals(pj2));
        assertFalse(pj1.equals(pj3));
        assertFalse(pj1.equals(pj4));
        assertTrue(pj1.equals(pj5));
        
    }
    
    @Test
    public void costTest() {
        // Arrange
        PrintJobBase pj1 = new A4PrintJob(10, 10, true);
        PrintJobBase pj2 = new A4PrintJob(10, 10, false);
        PrintJobBase pj3 = new A4PrintJob(10, 20, true);
        PrintJobBase pj4 = new A4PrintJob(0, 0, false);
        PrintJobBase pj5 = new A4PrintJob(10, 1000000, true);
        
        // Act & Assert
        assertEquals(3.00, pj1.totalCost(), 0.001);
        assertEquals(4.00, pj2.totalCost(), 0.001);
        assertEquals(5.00, pj3.totalCost(), 0.001);
        assertEquals(0.00, pj4.totalCost(), 0.001);
        assertEquals(200001.00, pj5.totalCost(), 0.001);
    }
    
    @Test
    public void toStringTest() {
        // Arrange
        PrintJobBase pj1 = new A4PrintJob(10, 10, true);
        PrintJobBase pj5 = new A4PrintJob(10, 1000000, true);
        
        String pj1String = "Print Job Details:\n" +
                "    Page Size:  A4\n" +
                "    Total Pages:  " + 20 + "\n" +
                "    Black & White Pages:  " + 10 + "\n" +
                "    Colour Pages:  " + 10 + "\n" +
                "    Double Sided:  " + true + "\n" +
                "    Total Cost:  $" + 3.00 + "\n";
        
        String pj5String = "Print Job Details:\n" +
                "    Page Size:  A4\n" +
                "    Total Pages:  " + 1000010 + "\n" +
                "    Black & White Pages:  " + 10 + "\n" +
                "    Colour Pages:  " + 1000000 + "\n" +
                "    Double Sided:  " + true + "\n" +
                "    Total Cost:  $" + 200001.00 + "\n";
        
        // Act & Assert
        assertEquals(pj1String, pj1.toString());
        assertEquals(pj5String, pj5.toString());
    }
}
