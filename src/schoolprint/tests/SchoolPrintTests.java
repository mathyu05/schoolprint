package schoolprint.tests;

import static org.junit.Assert.*;
import org.junit.Test;
import schoolprint.*;
import schoolprint.PrintJobs.A4PrintJob;
import schoolprint.PrintJobs.PrintJobBase;

/**
 * @author MatthewBaldwin
 *
 */
public class SchoolPrintTests {

    @Test
    public void openBadFileTest() {
        // Arrange
        SchoolPrint sp = new SchoolPrint();
        
        // Act
        boolean canOpenFileResult = sp.parseFile("Not a file...");
        
        // Assert
        assertFalse(canOpenFileResult);
    }
    
    @Test
    public void openGoodFileTest() {
        // Arrange
        SchoolPrint sp = new SchoolPrint();
        
        // Act
        boolean canOpenFileResult = sp.parseFile("src/schoolprint/tests/testData/sample.csv");
        
        // Assert
        assertTrue(canOpenFileResult);
    }
    
    @Test
    public void A4SinglesFileTest() {
        // Arrange
        SchoolPrint sp = new SchoolPrint();
        
        // Act
        boolean parsingResult = sp.parseFile("src/schoolprint/tests/testData/A4Singles.csv");
        
        // Assert
        assertTrue(parsingResult);
    }
    
    @Test
    public void openEmptyFileTest() {
        // Arrange
        SchoolPrint sp = new SchoolPrint();
        
        // Act
        boolean parsingResult = sp.parseFile("src/schoolprint/tests/testData/EmptyFile.csv");
        
        // Assert
        assertFalse(parsingResult);
    }
    
    @Test
    public void openBadDataFileTest() {
        // Arrange
        SchoolPrint sp = new SchoolPrint();
        
        // Act
        boolean parsingResult = sp.parseFile("src/schoolprint/tests/testData/badData.csv");
        
        // Assert
        assertFalse(parsingResult);
    }
    
    @Test
    public void openBadBoolFileTest() {
        // Arrange
        SchoolPrint sp = new SchoolPrint();
        
        // Act
        boolean parsingResult = sp.parseFile("src/schoolprint/tests/testData/badBool.csv");
        
        // Assert
        assertFalse(parsingResult);
    }
    
    @Test
    public void openJustCommasFileTest() {
        // Arrange
        SchoolPrint sp = new SchoolPrint();
        
        // Act
        boolean parsingResult = sp.parseFile("src/schoolprint/tests/testData/justCommas.csv");
        
        // Assert
        assertFalse(parsingResult);
    }
    
    @Test
    public void openMissingDataFileTest() {
        // Arrange
        SchoolPrint sp = new SchoolPrint();
        
        // Act
        boolean parsingResult = sp.parseFile("src/schoolprint/tests/testData/missingData.csv");
        
        // Assert
        assertFalse(parsingResult);
    }
    
    @Test
    public void openOnlyHeadersFileTest() {
        // Arrange
        SchoolPrint sp = new SchoolPrint();
        
        // Act
        boolean parsingResult = sp.parseFile("src/schoolprint/tests/testData/onlyHeaders.csv");
        
        // Assert
        assertFalse(parsingResult);
    }
    
    @Test
    public void openSpacedHeadersFileTest() {
        // Arrange
        SchoolPrint sp = new SchoolPrint();
        
        // Act
        boolean parsingResult = sp.parseFile("src/schoolprint/tests/testData/spacedHeaders.csv");
        
        // Assert
        assertTrue(parsingResult);
    }
    
    @Test
    public void newA4PrintJobTest() {
        // Arrange
        SchoolPrint sp = new SchoolPrint();
        
        PrintJobBase pj = new A4PrintJob(25 - 10, 10, false);
        
        // Act
        sp.parseFile("src/schoolprint/tests/testData/singleJob.csv");
        
        // Assert
        assertEquals(1, sp.getPrintJobs().size());
        assertEquals(pj, sp.getPrintJobs().get(0));
    }
    
    @Test
    public void sampleA4PrintJobTest() {
        // Arrange
        SchoolPrint sp = new SchoolPrint();
        
        PrintJobBase pj1 = new A4PrintJob(25 - 10, 10, false);
        PrintJobBase pj2 = new A4PrintJob(55 - 13, 13, true);
        PrintJobBase pj3 = new A4PrintJob(502 - 22, 22, true);
        PrintJobBase pj4 = new A4PrintJob(1 - 0, 0, false);
        
        // Act
        sp.parseFile("src/schoolprint/tests/testData/sample.csv");
        
        // Assert
        assertEquals(4, sp.getPrintJobs().size());
        assertEquals(pj1, sp.getPrintJobs().get(0));
        assertEquals(pj2, sp.getPrintJobs().get(1));
        assertEquals(pj3, sp.getPrintJobs().get(2));
        assertEquals(pj4, sp.getPrintJobs().get(3));
    }
    
    @Test
    public void samplePrintJobTotalCostTest() {
        // Arrange
        SchoolPrint sp = new SchoolPrint();
            
        sp.parseFile("src/schoolprint/tests/testData/sample.csv");
        
        // Act & Assert
        assertEquals(64.10, sp.totalCost(), 0.001);
    }
    
    @Test
    public void toStringTest() {
        // Arrange
        SchoolPrint sp = new SchoolPrint();
        
        PrintJobBase pj1 = new A4PrintJob(25 - 10, 10, false);
        PrintJobBase pj2 = new A4PrintJob(55 - 13, 13, true);
        PrintJobBase pj3 = new A4PrintJob(502 - 22, 22, true);
        PrintJobBase pj4 = new A4PrintJob(1 - 0, 0, false);
        
        sp.parseFile("src/schoolprint/tests/testData/sample.csv");
        
        String expectedResult = "\nPRINT JOB #" + 1 + "\n    " + pj1.toString() +
                                "\nPRINT JOB #" + 2 + "\n    " + pj2.toString() +
                                "\nPRINT JOB #" + 3 + "\n    " + pj3.toString() +
                                "\nPRINT JOB #" + 4 + "\n    " + pj4.toString() +
                                "\nTOTAL COST OF ALL PRINT JOBS:  $" + 64.10 + "\n";
        
        // Act & Assert
        assertEquals(expectedResult, sp.toString());
    }
    
    @Test
    public void equalsTest() {
        // Arrange
        SchoolPrint sp1 = new SchoolPrint();
        SchoolPrint sp2 = new SchoolPrint();
        SchoolPrint sp3 = new SchoolPrint();

        sp1.parseFile("src/schoolprint/tests/testData/sample.csv");
        sp2.parseFile("src/schoolprint/tests/testData/singleJob.csv");
        sp3.parseFile("src/schoolprint/tests/testData/sample.csv");
        
        // Act & Assert
        assertTrue(sp1.equals(sp1));
        assertFalse(sp1.equals(sp2));
        assertTrue(sp1.equals(sp3));
    }

}
