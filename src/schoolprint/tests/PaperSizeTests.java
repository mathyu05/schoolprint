package schoolprint.tests;

import static org.junit.Assert.*;
import java.io.IOException;
import org.junit.Test;
import schoolprint.PrintJobs.PaperSize;

/**
 * @author MatthewBaldwin
 *
 */
public class PaperSizeTests {

    @Test
    public void parseStringTests() {
        // Arrange
        String p1 = "A4";
        String p2 = "asdasd"; 
        
        // Act & Assert
        try {
            assertEquals(PaperSize.A4, PaperSize.parseString(p1));
        } catch (IOException e) {}
        
        try {
            PaperSize.parseString(p2);
            // force a failure
            assertTrue(false);
        } catch (IOException e) {
            // Correctly thrown exception
            assertTrue(true);
        }
    }
    
    @Test
    public void toStringTests() {
        // Arrange
        PaperSize paperSize = PaperSize.A4;
        
        // Act & Assert
        assertEquals("A4", paperSize.toString());
    }
}
