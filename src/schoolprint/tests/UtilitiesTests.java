package schoolprint.tests;

import static org.junit.Assert.*;
import java.io.IOException;
import org.junit.Test;
import schoolprint.*;
import schoolprint.PrintJobs.*;
import schoolprint.interfaces.IPrintJob;

/**
 * @author MatthewBaldwin
 *
 */
public class UtilitiesTests {

    @Test
    public void rountTest() {
        // Arrange
        double bigDecimal = 21.000000000000001;
        double expectedResult = 21.00;
        
        // Act
        double roundedDecimal = Utilities.round(bigDecimal, 2);
        
        // Assert
        assertEquals(expectedResult, roundedDecimal, 0.001);
    }
    
    @Test
    public void parseBoolTest() {
        // Arrange
        String goodTruthyBoi = "TRuE";
        String goodFalseyBoi = "fALse";
        String badBoi = "WhatchaGonnaDo";
        
        // Act & Assert
        try {
            assertTrue(Utilities.parseBool(goodTruthyBoi));
            assertFalse(Utilities.parseBool(goodFalseyBoi));
        } catch (IOException e) {}
        
        try {
            Utilities.parseBool(badBoi);
            // Force a failure
            assertTrue(false);
        } catch (IOException e) {
            // Thrown correctly
            assertTrue(true);
        }
    }
    
    @Test
    public void trimWhiteSpaceTest() {
        // Arrange
        String[] headers = {"col1", " col2", "col3 ", " col4 "};
        
        String[] trimmedHeaders = {"col1", "col2", "col3", "col4"};
        
        // Act & Assert
        assertArrayEquals(trimmedHeaders, Utilities.trimWhiteSpace(headers));
    }
    
    @Test
    public void printJobChooserTests() {
        // Arrange
        int colorPages = 55;
        int blackWhitePages = 33;
        boolean doubleSided = true;
        PaperSize paperSize = PaperSize.A4;
        
        IPrintJob expectedResult = new A4PrintJob(blackWhitePages, colorPages, doubleSided);
        
        // Act & Assert
        assertEquals(expectedResult, Utilities.printJobChooser(blackWhitePages, colorPages, doubleSided, paperSize));
    }

}
