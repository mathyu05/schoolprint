package schoolprint;

import java.io.IOException;
import java.math.*;

import schoolprint.PrintJobs.A4PrintJob;
import schoolprint.PrintJobs.PaperSize;
import schoolprint.interfaces.IPrintJob;


/**
 * @author MatthewBaldwin
 *
 * A static utilities class for general use throughout the program.
 */
public class Utilities {
    
    /**
     * Rounds doubles to the number of decimal places required.
     * 
     * @param value The double to be rounded.
     * @param places The number of decimal places required.
     * 
     * @return The rounded value.
     */
    public static double round(double value, int places) {
        
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bigDecimal = new BigDecimal(value);
        bigDecimal = bigDecimal.setScale(places, RoundingMode.HALF_UP);
        
        return bigDecimal.doubleValue();
    }
    
    /**
     * A robust string to boolean parser which throws an exception if the parsed string is not directly convertible.
     * 
     * @param potentialBool The string to be converted to a boolean.
     * @return The value of the boolean converted.
     * @throws IOException Thrown when potentialBool is certainly not a potential boolean.
     */
    public static boolean parseBool (String potentialBool) throws IOException {
        if (potentialBool.equalsIgnoreCase("true") || potentialBool.equalsIgnoreCase("false")) {
            return Boolean.valueOf(potentialBool);
        } else {
            throw new IOException();
        }
    }
    
    /**
     * Trim whitespace from all strings within an array.
     * 
     * @param data An array of strings.
     * @return The array of trimmed strings.
     */
    public static String[] trimWhiteSpace(String[] data) {
        
        for (int i = 0; i < data.length; ++i) {
            data[i] = data[i].trim();
        }
        
        return data;
    }
    
    /**
     * Method for choosing which subclass of PrintJobBase to use for the Print Job at hand. 
     * 
     * @param colorPages The number of colour pages.
     * @param blackWhitePages The number of black and white pages.
     * @param doubleSided Flag determining whether to print double sided
     * @param paperSize The size of the paper.
     * @return a new IPrintJob.
     */
    public static IPrintJob printJobChooser (int blackWhitePages, int colorPages, boolean doubleSided, PaperSize paperSize) {
        switch (paperSize) {
            case A4:
                return new A4PrintJob(blackWhitePages, colorPages, doubleSided);
            default:
                return new A4PrintJob(blackWhitePages, colorPages, doubleSided);
        }
    }
}
