# School Print (SCHLPRNT) #

To reduce printing waste, a school is implementing Printer Management software and will charge for printing for printing as follows:

* Paper size A4, job type single sided:
    * 15c / bw page
    * 25c / colour page
* Paper size A4, job type double sided:
    * 10c / bw page
    * 20c / colour page

This program will help the system administrator to calculate print costs.  It takes a list of A4 print jobs and calculates the cost of each job given the total number of pages, number of colour pages, and whether printing is double sided.

### Works ###

* Read print jobs from a file
* Output the job details and job cost for each job to the console
* Output the total cost of all jobs to the console

### Future Works (out of scope for the moment) ###

* Support for page types other than A4

### Programming Language ###

* Written in Java

### Methods ###

* Develop using a Test Driven approach
* Develop off of private/feature branches and then merge back into master after review.